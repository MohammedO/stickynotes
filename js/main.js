import db from './database.js';
import UIUtility from './UI.js';

class Note {
    constructor(desc = "", color = 0, size = [200, 210], pos = [100, 100]) {
        this.date = Date.now();
        this.desc = desc;
        this.color = color;
        this.size = size;
        this.pos = pos;
    }
    
    render() {
		return `
            <div class="note draggable" style = "background: ${noteColors[this.color]}; width: ${this.size[0]}px; height: ${this.size[1]}px; top: ${this.pos[0]}px; left: ${this.pos[1]}px" tabindex="0"  onmouseup="changeNoteSize(${this.date}, this)">
                <div class="desc" contenteditable="true" onkeyup="changeDesc(${this.date}, this)">${this.desc}</div>
                <div class="edit-tools">
                    <div class="colors">
                        <div class="color default" onclick="changeNoteColor(${this.date}, this.parentElement.parentElement.parentElement, 0)"></div>
                        <div class="color red" onclick="changeNoteColor(${this.date}, this.parentElement.parentElement.parentElement, 1)"></div>
                        <div class="color green" onclick="changeNoteColor(${this.date}, this.parentElement.parentElement.parentElement, 2)"></div>
                        <div class="color blue" onclick="changeNoteColor(${this.date}, this.parentElement.parentElement.parentElement, 3)"></div>
                    </div>
                    <div class="delete-button" onclick="deleteNote(${this.date}, this.parentElement.parentElement)"></div>
                </div>
            </div>
        `;
    }
}

class Board  {
    constructor() {
        this.name = "test";
        this.date = Date.now();
        this.notes = [];
        this.counter = 0;
    }

    addNote() {
        this.notes[this.counter] = new Note();
        this.counter++;
    }

    updateNoteColor(key, color) {
        let targetNote = this.notes.find(obj => {return obj.date === key});
        targetNote.color = color;
    }

    updateNoteSize(key, pos, size) {
        let targetNote = this.notes.find(obj => {return obj.date === key});

        targetNote.pos = pos;
        targetNote.size = size;
    }

    removeNote(key) {
        this.notes = this.notes.filter(obj => obj.date !== key);
        this.counter--;
    }

    updateDesc(key, desc) {
        let targetNote = this.notes.find(obj => {return obj.date === key});
        targetNote.desc = desc;
    }

    render() {
        let res = "";

        this.notes.forEach(function (note) {
            res += note.render();
        });

        return res;
    }
}

class BoardManager {
	constructor() {
        this.boards = new Map();
        this.selectedBoard = -1;
        this.load();
        this.updateList();
        this.updateBody();
	}

	addBoard() {
		let newBoard = new Board();
        this.boards.set(newBoard.date,newBoard);

        if(this.selectedBoard == -1) {
            this.selectedBoard = newBoard.date;
            this.switchBoard(this.selectedBoard);
        }

        this.updateList();
		this.save();
    }
    
    switchBoard(id) {
        this.selectedBoard = id;
        this.updateBody();
        this.updateList();
        this.save();
    }

	removeBoard(id) {
		this.save();
	}

	addNote() {
        if(this.selectedBoard < 0) {
            window.alert("Please Add Boards First");
            return;
        }
        this.boards.get(this.selectedBoard).addNote();
        this.updateBody();
		this.save();
	}

	save() {
        db.setItem("boards",Array.from(this.boards));
        db.setItem("Selectedboard",this.selectedBoard);
	}

	load() {
        this.boards = new Map(db.getItem("boards"));

        this.boards.forEach(function(value,key,m) {
            value.notes.forEach((value,index,array) => {
                array[index] = dateObjectCast(value,new Note());
            });
            m.set(key,dateObjectCast(value,new Board()));
        });

        this.selectedBoard = db.getItem("Selectedboard")? db.getItem("Selectedboard") : -1;
	}

	updateList() {
        let list = "";
        let currentBoard = this.selectedBoard;
		this.boards.forEach(function(value,key)  {
            let selected = currentBoard === key ? "selected" : "";
			list += `<li onclick="switchBoard(${key}, this)" class="${selected}">${value.name}</li>`;
		});
       
        document.getElementById("boards-list").innerHTML = list;
    }
    
    updateBody() {
        if(this.selectedBoard < -1) {
            window.alert("Please Add Boards First");
            return;
        }
        
        if(!this.boards.has(this.selectedBoard)) {
            return;
        }

        document.getElementById("note-list").innerHTML = this.boards.get(this.selectedBoard).render();
    }

    getSelectedBoard() {
        return this.boards.get(this.selectedBoard);
    }
}


UIUtility();

let dateObjectCast = (data,object) =>
{
    for(let k in data) object[k]=data[k];
    return object;
}



let noteColors = ["#EFEFEF","#F65151","#CDFCB6","#B6D7FC"];

window.onload = () => {
    let mainManger = new BoardManager();

    // Get elements
    let newNoteBtn = document.getElementById("new-note");
    let newBoardBtn = document.getElementById("new-board");
    let newBoardTitle = document.getElementById("newBoardTitle");

	newNoteBtn.onclick = () => {
        mainManger.addNote();
    }

	newBoardBtn.onclick = () =>  {
        newBoardTitle.innerHTML = "";
		mainManger.addBoard();
    };

    newBoardBtn.onfocusout = () => {
        newBoardTitle.innerHTML = "New Board";
    }

    newBoardTitle.onchange = () => {
        console.log(newBoardTitle.innerHTML);
    }
    
    window.switchBoard = (id, element) => {
        mainManger.switchBoard(id);
    }

    window.changeNoteColor = (id, element, colorIndex) => {
        element.style.backgroundColor = noteColors[colorIndex];
        mainManger.getSelectedBoard().updateNoteColor(id, colorIndex);
        mainManger.save();
    }
    
    window.deleteNote = (id, element) => {
        element.parentElement.removeChild(element);
        mainManger.getSelectedBoard().removeNote(id);
        mainManger.save();
    }
    
    window.changeNoteSize = (id, element) => {
        mainManger.getSelectedBoard().updateNoteSize(id, [element.offsetTop, element.offsetLeft], [element.offsetWidth, element.offsetHeight]);
        console.log([element.offsetTop, element.offsetLeft]);
        mainManger.save();
    }

    window.changeDesc = (id, element) => {
        mainManger.getSelectedBoard().updateDesc(id, element.innerHTML);
        mainManger.save();
    }
}

