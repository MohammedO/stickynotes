export default class Database {
    static getItem(key) {
        return JSON.parse(localStorage.getItem(key));
    }

    static getItemObject(key,object) {
        let item = this.getItem(key);
        console.log(item);
        for(let k in item) object[k]=item[k];
        return object;
    }

    static setItem(key,item) {
        const jsonItem = JSON.stringify(item);
        localStorage.setItem(key,jsonItem);
    }
}