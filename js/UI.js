// UI utility 

// Mouse Tracking 
var x = null;
var y = null;
    

function trackMouse(e)
{
  x = e.pageX;
  y = e.pageY;
}

// get Mouse Position
export function getMouseX() 
{
    return x;
}

export function getMouseY()
{
    return y;
}

// related to drag and drop 
let shiftX = 0,shiftY = 0;

// tracking
let draggable;

let dragStart = (element) =>
{
    draggable = document.activeElement;

    if(draggable && document.activeElement.classList.contains("draggable"))
    {
        draggable.style.zIndex = "1";
    }

    shiftX = getMouseX() - (parseInt(draggable.style.left) || 0);
    shiftY = getMouseY() - (parseInt(draggable.style.top) || 0);
        
}

let DragMove = (element) =>
{
    if(draggable) draggable = document.activeElement;

    if(draggable && document.activeElement.classList.contains("draggable"))
    {
        draggable.style.top = (element.pageY - shiftY)+ "px";
        draggable.style.left = (element.pageX  - shiftX) + "px";
        
    } 
}

let clearDrag = () =>
{
    if(draggable && document.activeElement.classList.contains("draggable"))
    {
        draggable.style.zIndex = "0";
        draggable.blur();
    }

    draggable = null;
}



export default function setUpUI()
{
    // set up for mouse tracking
    document.addEventListener('mousemove', trackMouse, false);
    document.addEventListener('mouseenter', trackMouse, false);

    // set up for dragging objects
    document.body.addEventListener('focusin',dragStart);
    document.body.addEventListener('mousemove',DragMove);
    document.body.addEventListener('mouseup',clearDrag);
    document.body.addEventListener('mouseleave',clearDrag);
}